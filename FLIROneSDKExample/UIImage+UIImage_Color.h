//
//  UIImage+UIImage_Color.h
//  FLIROneSDKExampleApp
//
//  Created by NganKam Pong on 22/11/2015.
//  Copyright (c) 2015年 novacoast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_Color)
- (UIColor *)colorAtPixel:(CGPoint)point;
@end
