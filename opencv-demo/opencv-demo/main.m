//
//  main.m
//  opencv-demo
//
//  Created by Joseph Colicchio on 5/4/15.
//  Copyright (c) 2015 Novacoast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
