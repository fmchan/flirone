//
//  ViewController.m
//  networking-demo
//
//  Created by Joseph Colicchio on 4/27/15.
//  Copyright (c) 2015 asdf2. All rights reserved.
//

#import "ViewController.h"
#import "FLIROneSDK/FLIROneSDK.h"

@interface ViewController () <NSStreamDelegate, FLIROneSDKImageReceiverDelegate, FLIROneSDKStreamManagerDelegate>

@property (strong, nonatomic) NSInputStream *inputStream;
@property (strong, nonatomic) NSOutputStream *outputStream;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label;

@property (strong, nonatomic) dispatch_queue_t networkQueue;

@end

@implementation ViewController

BOOL connected = false;
BOOL writing = false;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // add this view controller as a delegate
    [[FLIROneSDKStreamManager sharedInstance] addDelegate:self];
    
    // create the network queue for sending image data, optionally used to ensure only one write at a time
    self.networkQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // set initial image options
    [[FLIROneSDKStreamManager sharedInstance] setImageOptions:
     FLIROneSDKImageOptionsThermalRGBA8888Image | FLIROneSDKImageOptionsThermalLinearFlux14BitImage];
}

// when we receive a 120x160 thermal data only image, send it
- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveThermal14BitLinearFluxImage:(NSData *)linearFluxImage imageSize:(CGSize)size {
    UIImage *renderedImage = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsThermalLinearFlux14BitImage andData:linearFluxImage andSize:size];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageView setImage:renderedImage];
    });
    
    if(!writing) {
        [self sendImage:renderedImage];
    }
}

// this is the msx version, 480x640 colorized with visual data, send it as well
// only one of these two options should be enabled at any given time
- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveBlendedMSXRGBA8888Image:(NSData *)msxImage imageSize:(CGSize)size {
    
    UIImage *renderedImage = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsBlendedMSXRGBA8888Image andData:msxImage andSize:size];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageView setImage:renderedImage];
    });

    if(!writing) {
        [self sendImage:renderedImage];
    }
}


// connect to the python script over local network
// we use arbitrary host/port of localhost and 8888, but you should match the output of the python script
- (void) initConnection {
    NSString *host = @"127.0.0.1";
    int port = 8888;
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)host, port, &readStream, &writeStream);
    self.inputStream = (__bridge NSInputStream *)readStream;
    self.outputStream = (__bridge NSOutputStream *)writeStream;
    
    [self.inputStream setDelegate:self];
    [self.outputStream setDelegate:self];
    
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [self.inputStream open];
    [self.outputStream open];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.label setText:@"connected to python script"];
    });

    connected = YES;
}

// send an image to the connected python script
- (void) sendImage:(UIImage *)image {
    // don't enqueue multiple writes in order to prevent network lag
    writing = YES;
    
    //
    dispatch_async(self.networkQueue, ^{
        // first time, set up connection
        // NOTE: make sure the python script is running
        if(!connected) {
            [self initConnection];
        }
        
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        
        // save the length in 4 bytes
        uint32_t length = (uint32_t)[imageData length];
        uint8_t lengthBytes[4];
        uint32_t *rawLength = (uint32_t *)lengthBytes;
        rawLength[0] = length;
        
        // swap byte order of uint32_t to match python script
        uint8_t temp = lengthBytes[0];
        lengthBytes[0] = lengthBytes[3];
        lengthBytes[3] = temp;
        temp = lengthBytes[1];
        lengthBytes[1] = lengthBytes[2];
        lengthBytes[2] = temp;
        
        
        // actually send the data now, 4 bytes of length then N bytes of payload
        int totalLength = 0;
        while(totalLength < 4) {
            totalLength += [self.outputStream write:(lengthBytes+totalLength) maxLength:4-totalLength];
        }
        
        totalLength = 0;
        while(totalLength < length) {
            totalLength += [self.outputStream write:([imageData bytes]+totalLength) maxLength:length-totalLength];
        }
        
        writing = false;
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)toggleFormats:(id)sender {
    
    if([[FLIROneSDKStreamManager sharedInstance] imageOptions] & FLIROneSDKImageOptionsBlendedMSXRGBA8888Image) {
        [[FLIROneSDKStreamManager sharedInstance] setImageOptions:
         FLIROneSDKImageOptionsThermalRGBA8888Image | FLIROneSDKImageOptionsThermalLinearFlux14BitImage];
    } else {
        [[FLIROneSDKStreamManager sharedInstance] setImageOptions:
         FLIROneSDKImageOptionsBlendedMSXRGBA8888Image];
    }
}

@end
