//
//  AppDelegate.h
//  networking-demo
//
//  Created by Joseph Colicchio on 4/27/15.
//  Copyright (c) 2015 asdf2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

